# CloudAPI for OpenAPI dev & API documentation

*spin up the server:*
1. start PostgreSQL service `sudo service postgresql start`, or `brew services start mongodb` for MacOS connect to the service `psql`; cr8 database: `CREATE DATABASE "foodie";`
2. start cloudAPI server: `node .`

Executive SQL queries:

To enter Postgres SQL query, do `psql`

`DROP DATABASE "foodie";`

`CREATE DATABASE "foodie";`

`\l` : show databases;

`\c foodie` : use database name 'foodie'; MySQL: `use foodie`;

`\dt` : show tables (all) in a database;

`\d appuser;` : describe <table>

User: postgres
