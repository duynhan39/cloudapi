var server = require('../server');
var ds = server.dataSources.db;
// var lbTables = ['User', 'AccessToken', 'ACL', 'RoleMapping', 'Role', 'appUser', 'foodieUser',  'foodieProfile', 'foodieUserTransaction', 'app'];
var lbTables = ['User', 'AccessToken', 'ACL', 'RoleMapping','Role', 'foodieUser', 'appUser'];
ds.automigrate(lbTables, function(er) {
  if (er) throw er;
  console.log('Loopback tables [' + lbTables + '] created in ', ds.adapter.name);
  ds.disconnect();
});
